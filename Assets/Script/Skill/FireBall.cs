using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    [SerializeField] CharacterScriptableObject character;
    [SerializeField] GameObject fireZoneDebuff;

    private void Start()
    {
        Destroy(gameObject,1.02f);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Enemy")
        {
            collision.GetComponent<Enemy>().TakeDamage((int)character.skillValue);
            TriggerFireZoneDebuff();
        }
    }
    public void TriggerFireZoneDebuff()
    {
        Instantiate(fireZoneDebuff, transform.position, Quaternion.identity);
    }
}
