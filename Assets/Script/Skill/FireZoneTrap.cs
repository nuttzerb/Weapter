using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireZoneTrap : MonoBehaviour
{
    [SerializeField] GameObject fireDebuff;
    private float ran;

    private void Start()
    {
        Destroy(gameObject, 3f);
        StartCoroutine(InstantiateFireDebuff());

    }
    void Update()
    {
        ran = Random.Range(-0.5f,0.5f);
    }
    IEnumerator InstantiateFireDebuff()
    {
        var fireDebuffObj = Instantiate(fireDebuff,this.transform);
        fireDebuffObj.transform.position = new Vector3(fireDebuffObj.transform.position.x + ran, fireDebuffObj.transform.position.y + ran);
        Destroy(fireDebuffObj, 0.8f);
        yield return new WaitForSeconds(.34f);
        StartCoroutine(InstantiateFireDebuff());

    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if (player != null)
        {
            player.GetComponent<Rigidbody2D>().WakeUp();
            if (!player.isTakingDamage)
                StartCoroutine(DelayTakingDamage(player, 1f));
        }
    }
    IEnumerator DelayTakingDamage(Player player, float duration)
    {
        if (player.tag == "Player")
        {
            player.TakeDamage(1);
            player.isTakingDamage = true;
            yield return new WaitForSeconds(duration);
            player.isTakingDamage = false;

        }
    }
}
