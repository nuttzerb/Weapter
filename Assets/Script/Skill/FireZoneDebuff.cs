using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireZoneDebuff : MonoBehaviour
{
    [SerializeField] GameObject fireDebuff;
    private float ran;

    private void Start()
    {
        Destroy(gameObject, 3f);
        StartCoroutine(InstantiateFireDebuff());

    }
    void Update()
    {
        ran = Random.Range(-0.5f,0.5f);
    }
    IEnumerator InstantiateFireDebuff()
    {
        var fireDebuffObj = Instantiate(fireDebuff,this.transform);
        fireDebuffObj.transform.position = new Vector3(fireDebuffObj.transform.position.x + ran, fireDebuffObj.transform.position.y + ran);
        Destroy(fireDebuffObj, 0.8f);
        yield return new WaitForSeconds(.34f);
        StartCoroutine(InstantiateFireDebuff());

    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.GetComponent<Rigidbody2D>().WakeUp();
            if (!enemy.isTakingDamage)
                StartCoroutine(DelayTakingDamage(enemy, 1f));
        }
    }
    IEnumerator DelayTakingDamage(Enemy enemy, float duration)
    {
        if (enemy.tag == "Enemy")
        {
            enemy.TakeDamage(1);
            enemy.isTakingDamage = true;
            yield return new WaitForSeconds(duration);
            enemy.isTakingDamage = false;

        }
    }
}
