using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : MonoBehaviour
{
    public Weapon weapon;
    SpriteRenderer spriteRenderer;

    Weapon tempWeapon;
    SpriteRenderer tempSpriteWeapon;
    SpriteRenderer playerWeaponSpriteRenderer;
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
         playerWeaponSpriteRenderer = GetComponent<SpriteRenderer>();
        tempSpriteWeapon = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = weapon.currentWeaponSprite;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            GameManager.instance.ShowText(weapon.name, 24, Color.white, transform.position, Vector3.one, 1f);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        collision.GetComponent<Rigidbody2D>().WakeUp();
        if (collision.tag == "Player" && GameManager.instance.GetVpadController().IsPickup())
        {
            GameManager.instance.GetVpadController().SetPickupButtonInteracable(true);
            Pickup(collision);
        }
    }
    
    private void Pickup(Collider2D collision)
    {
        playerWeaponSpriteRenderer = collision.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
        //bien tam
        tempWeapon = collision.GetComponent<Player>().currentWeapon;
        tempSpriteWeapon.sprite = playerWeaponSpriteRenderer.sprite;
        // nhat vu khi len
        collision.GetComponent<Player>().currentWeapon = weapon;
        playerWeaponSpriteRenderer.sprite = weapon.currentWeaponSprite;
        // bo vu khi xuong
        weapon = tempWeapon;
        spriteRenderer.sprite = tempSpriteWeapon.sprite;
        GameManager.instance.GetVpadController().OnClickPickup(false);
    }

}
