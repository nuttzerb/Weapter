using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Creature
{
    public PlayerHealthUI playerHealthUI;
    [Header("Dash")]
    public float dashRange = 50f;
    public float timeBetweenDash = 1f;
    private bool dashKey;
    private bool canDash = true;
    private bool isDashing = false;
    private Vector2 dashDir;
    [SerializeField] float distanceToFindEnemy=70;
    private bool isShielding;
    public bool isTakingDamage;
    public bool isAlive = true;

    public Weapon currentWeapon;
    [SerializeField] public CharacterScriptableObject currentCharacter;
    [SerializeField] SkillSystem skillSystem;
    [SerializeField] public WeaponController weaponController;
    private Vector3 movement;
    public Rigidbody2D rb;
    public BoxCollider2D boxCollider2D;
    public Animator animator;
    [SerializeField] FlashEffect flashEffect;

    private Enemy closestEnemy;
    public Vector3 lastMoveDirection;

    public int coins=0;
    public int levels=0;
    public int[] characterOwnedList;
    [Header("Audio")]
    public AudioSource audioSource;
    public AudioClip deadAudio;
    public AudioClip dashAudio;
    public AudioClip healAudio;

    protected override void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        playerHealthUI.SetMaxHealth(maxHitpoint);
        SpawnPlayerInPosition();
        InitPlayerCharacter();
        //  DontDestroyOnLoad(gameObject);
    }


    private void Update()
    {
        if(isAlive) TakeInput();
        FindClosestEnenmy();
        if(GameManager.instance.GetVpadController().IsSkillPressed())
        {
            PerformSkill(currentCharacter);
            GameManager.instance.GetVpadController().OnClickSkill(false);
            StartCoroutine(StartCooldown(currentCharacter.skillcooldown));
        }
    }
    void FixedUpdate() // co dinh
    {
        if (isAlive)
        {
            Move();
            if (canDash && dashKey)
            {
                animator.SetTrigger("Dash");

                isDashing = true;
                canDash = false;
                dashDir = new Vector2(lastMoveDirection .x, lastMoveDirection.y);
                if (dashDir == Vector2.zero)
                {
                    dashDir = new Vector2(transform.localScale.x, transform.localScale.y);
                }
                StartCoroutine(StopDashing());

            }
            if (isDashing)
            {
                rb.velocity = dashDir.normalized * dashRange;
                return;
            }
        }
    }

    private void InitPlayerCharacter()
    {
        spriteRenderer.sprite = currentCharacter.characterSprite;
        animator.runtimeAnimatorController = currentCharacter.characterAnim;
        if (GetTotalOwnedCharacter() == 0)
        {
            characterOwnedList = new int[GameManager.instance.characterManager.GetTotalCharacter()];
            characterOwnedList[(int)Character.Rogue] = 1;// add default character

                                                          }
    }
    private void TakeInput()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        movement = new Vector3(movement.x, movement.y).normalized; // do lon cua vector = 1 
        dashKey = Input.GetButton("Jump"); 
        //dashKey = Input.GetButton("Jump"); 
        animator.SetFloat("Walk", Mathf.Abs(movement.x + movement.y));
        if(Input.GetKeyDown(KeyCode.R))
        {
            characterOwnedList = new int[GameManager.instance.characterManager.GetTotalCharacter()];
            coins = 0;
            levels = 0;
            GameManager.instance.SavePlayer();
        }
    }
    IEnumerator StopDashing()
    {
        yield return new WaitForSeconds(timeBetweenDash);
        isDashing = false;
        canDash = true;
    }
    private void Move()
    {
        //di chuyen
        rb.velocity = new Vector3(movement.x * speed, movement.y * speed);
        if(movement!=Vector3.zero)
        {
            lastMoveDirection = movement;
        }
    }
/*    public void SwapSprite(int skinId)
    {
        spriteRenderer.sprite = GameManager.instance.playerSprites[skinId];
    }
*/
    protected override void Death()
    {
        //   audioSource.PlayOneShot(deadAudio);
        Invoke("ShowDeathMenu", 1f);

    }

    private void ShowDeathMenu()
    {
        isAlive = false;
        //  spriteRenderer.color = Color.black;

        GameManager.instance.menuCanvas.transform.GetChild(3).gameObject.SetActive(true);
        Time.timeScale = 0f;
    }

    /*    public void Respawn()   
   {
       isAlive = true;
       lastImmune = Time.time;
       pushDirection = Vector3.zero;
       boxCollider2D.enabled = true;

   }*/
    public void Heal(int healingAmount)
    {
        if (hitpoint == maxHitpoint)
        {
            return;
        }
        hitpoint += healingAmount;
        if (hitpoint > maxHitpoint)
        {
            hitpoint = maxHitpoint;
        }
        playerHealthUI.SetHealth(hitpoint);
        GameManager.instance.ShowText("+" + healingAmount.ToString() + "hp", 25, Color.red, transform.position, Vector3.up * 30, 0.7f);


        //  audioSource.PlayOneShot(healAudio);
    }
    public override void TakeDamage(int damage)
    {
        if (isShielding) return;
        base.TakeDamage(damage);
        playerHealthUI.SetHealth(hitpoint); // xem lai sau
        flashEffect.Flash();
    }

    private Enemy FindClosestEnenmy()
    {
        float distanceToClosestEnemy = Mathf.Infinity;
        closestEnemy = null;
        GameObject[] allEnemies =GameObject.FindGameObjectsWithTag("Enemy");
        if (allEnemies.Length != 0)
        {
            foreach (GameObject enemy in allEnemies)
            {
                if (enemy.GetComponent<Enemy>() != null)
                {
                    enemy.GetComponent<Enemy>().isClosestDistanceToPlayer = false;
                    float distanceToCloseEnemy = (enemy.transform.position - this.transform.position).sqrMagnitude;
                    if(distanceToCloseEnemy < distanceToFindEnemy)
                    {
                        if (distanceToCloseEnemy < distanceToClosestEnemy)
                        {
                            distanceToClosestEnemy = distanceToCloseEnemy;
                            closestEnemy = enemy.GetComponent<Enemy>();
                        }
                    }

                }

            }
            if(closestEnemy!=null)
              closestEnemy.isClosestDistanceToPlayer = true;

            return closestEnemy;
        }
        else return null;

    }
    public Enemy GetClosestEnemy()
    {
        return closestEnemy;
    }
    public void PerformSkill(CharacterScriptableObject characterData)
    {
        switch (characterData.characterName)
        {
            case "Rogue":
                PerformRogueSkill(characterData);
                break;
            case "Guardian":
                PerformGuardianSkill(characterData);
                break;
            case "Wizard":
                PerformWizardSkill(characterData);
                break;
        }
    }
    private void PerformRogueSkill(CharacterScriptableObject characterData)
    {
        Debug.Log("Perform Rogue Skill: " + characterData.skillKey);
        if (canDash)
        {
            animator.SetTrigger("Dash");

            isDashing = true;
            canDash = false;
            dashDir = lastMoveDirection;
            if (dashDir == Vector2.zero)
            {
                dashDir = lastMoveDirection;
            }
            StartCoroutine(StopDashing());

        }
        if (isDashing)
        {
            rb.velocity = dashDir.normalized * characterData.skillValue;
            return;
        }
    }
    private void PerformGuardianSkill(CharacterScriptableObject characterData)
    {
        Debug.Log("Perform Guardian Skill: " + characterData.skillKey);
        StartCoroutine(EnableGuardianShield(characterData.skillValue));
    }
    IEnumerator EnableGuardianShield(float duration)
    {
        isShielding = true;
        Debug.Log(spriteRenderer.name);
        spriteRenderer.color = new Color32(192,77, 192, 255);
        Debug.Log(spriteRenderer.color);
        yield return new WaitForSeconds(duration);
        spriteRenderer.color = new Color32(255, 255, 255,255);
        isShielding = false;
    }
    private void PerformWizardSkill(CharacterScriptableObject characterData)
    {
        Debug.Log("Perform Wizard Skill: " + characterData.skillKey);
        var Pos = GetClosestEnemy().transform.position;
        var direction = (Pos - transform.position).normalized;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rot = Quaternion.Euler(new Vector3(0f, 0f, angle ));
        GameObject fireBall= Instantiate(characterData.skillPrefab,transform.position, rot);
        fireBall.GetComponent<Rigidbody2D>().velocity = direction *characterData.skillValue;
    }
    IEnumerator StartCooldown(float cooldown)
    {
        GameManager.instance.GetVpadController().skillButtonEventTrigger.interactable=false;
        yield return new WaitForSeconds(cooldown);
        GameManager.instance.GetVpadController().skillButtonEventTrigger.interactable = true;
    }
    public int GetTotalOwnedCharacter()
    {
        int count = 0;
        for (int i = 0; i < characterOwnedList.Length; i++)
        {
            if (characterOwnedList[i] == 1) count++;
        }
        return count;
    }
    public void SpawnPlayerInPosition()
    {
        transform.position = GameObject.FindGameObjectWithTag("StartPoint").transform.position;
    }
}
