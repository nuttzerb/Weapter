using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    [SerializeField] List<CharacterScriptableObject> _characterScriptableObjects;

    public CharacterScriptableObject GetCharacterData(string characterName)
    {
        return _characterScriptableObjects.Find(e => e.characterName == characterName);
    }
    public int GetTotalCharacter()
    {
        return _characterScriptableObjects.Count();
    }
    public int GetCharacterIndex(string characterName)
    {
        string name = GetCharacterData(characterName).characterName;
        int i;
        for (i = 0; i < GetTotalCharacter(); i++)
        {
            if(name == ((Character)i).ToString())
            {
                break;
            }
        }
        return i;
    }
}
