using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "Character")]
public class CharacterScriptableObject : ScriptableObject
{

    [SerializeField] string _characterName;
    public string characterName
    {
        get { return _characterName; }
        private set { }
    } 
    [SerializeField] string _characterPrice;
    public string characterPrice
    {
        get { return _characterPrice; }
        private set { }
    }
    [SerializeField] Sprite _characterSprite;
    public Sprite characterSprite
    {
        get { return _characterSprite; }
        private set { }
    }
    [SerializeField] RuntimeAnimatorController _characterAnim;
    public RuntimeAnimatorController characterAnim
    {
        get { return _characterAnim; }
        private set { }
    }
    [SerializeField] string _skillKey;
    public string skillKey
    {
        get { return _skillKey; }
        private set { }
    }
    [SerializeField] float _skillValue;
    public float skillValue
    {
        get { return _skillValue; }
        private set { }
    }  
    [SerializeField] float _skillcooldown;
    public float skillcooldown
    {
        get { return _skillcooldown; }
        private set { }
    }
    [SerializeField] GameObject _skillPrefab;
    public GameObject skillPrefab
    {
        get { return _skillPrefab; }
        private set { }
    }
}
