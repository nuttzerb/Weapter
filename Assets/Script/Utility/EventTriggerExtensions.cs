using UnityEngine.Events;
using UnityEngine.EventSystems;

public static class EventTriggerExtensions 
{
    public static void AddListener(this EventTrigger eventTrigger, EventTriggerType eventTriggerType, UnityAction<BaseEventData> callback)
    {
        if (eventTrigger == null || callback == null)
        {
            return;
        }

        EventTrigger.Entry entry = eventTrigger.triggers.Find(e => e.eventID == eventTriggerType);
        if (entry == null)
        {
            entry = new EventTrigger.Entry();
            entry.eventID = eventTriggerType;
            entry.callback.AddListener(callback);
            eventTrigger.triggers.Add(entry);
        }
    }

    public static void RemoveListener(this EventTrigger eventTrigger, EventTriggerType eventTriggerType, UnityAction<BaseEventData> callback)
    {
        if (eventTrigger == null || callback == null)
        {
            return;
        }

        EventTrigger.Entry entry = eventTrigger.triggers.Find(e => e.eventID == eventTriggerType);
        if (entry != null)
        {
            entry.callback.RemoveListener(callback);
        }
    }

    public static void RemoveAllListeners(this EventTrigger eventTrigger, EventTriggerType eventTriggerType)
    {
        if (eventTrigger == null)
        {
            return;
        }

        EventTrigger.Entry entry = eventTrigger.triggers.Find(e => e.eventID == eventTriggerType);
        if (entry != null)
        {
            entry.callback.RemoveAllListeners();
        }
    }
}
