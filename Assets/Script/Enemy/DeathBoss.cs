using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBoss : Enemy
{
    [HideInInspector] public float attackDistance;
    [HideInInspector] public float spellDistance;
    [SerializeField] private GameObject spell;
    [SerializeField] private GameObject portal;
    float originalSpeed;
    bool canCast=true;
    bool enableCast = false;
    protected override void Start()
    {
        base.Start();
        GameManager.instance.bossHealthSlider.gameObject.SetActive(true);
        rb = GetComponent<Rigidbody2D>();
        attackDistance = base.stoppingDistance + 1.5f;
        spellDistance = base.stoppingDistance + 10;
        originalSpeed = speed;

    }

    protected override void Update()
    {
        base.Update();
        animator.SetBool("Walk",canMove);

        GameManager.instance.bossHealthSlider.maxValue = maxHitpoint;
        GameManager.instance.bossHealthSlider.value = hitpoint;



        BossMechanic();
    }
    protected override void Death()
    {
        StopMove();
        GetComponent<BoxCollider2D>().enabled = false;

        if (portal!=null)
        portal.gameObject.SetActive(true);
        animator.SetTrigger("Dead");
        Destroy(gameObject, 1f);
        GameManager.instance.bossHealthSlider.gameObject.SetActive(false);
    }
    private void BossMechanic()
    {
        if (hitpoint <= maxHitpoint / 2)
        {
            if (!enableCast)
            {
                Invoke(nameof(EnableCastDelay),1f);
            }

            if (Vector2.Distance(transform.position, base.player.position) > attackDistance && Vector2.Distance(transform.position, base.player.position) <= spellDistance)
            {
                if (canCast && enableCast)
                {
                    StartCoroutine(SpellCastDelay(1f));
                }
            }
        }
        if (Vector2.Distance(transform.position, base.player.position) <= attackDistance)
        {
            StartCoroutine(AttackDelay());
        }
        else if (Vector2.Distance(transform.position, base.player.position) >= attackDistance)
        {
            StopAttack();
        }



    }
    void EnableCastDelay()
    {
        animator.SetTrigger("Cast");
        enableCast = true;
    }

    IEnumerator SpellCastDelay(float duration)
    {
        canCast = false; 
        yield return new WaitForSeconds(1f);
        OnPerformSpell();
        yield return new WaitForSeconds(duration);
        canCast = true;
        
    }
    private void OnPerformSpell()
    {

        Vector3 position = new Vector3(player.transform.position.x, player.transform.position.y + 1, player.transform.position.z);
        Instantiate(spell, position, Quaternion.identity);
    }
    private void OnPerformSpell(int quantity )
    {
        for (int i = 0; i < 11; i++)
        {
            var ranX = UnityEngine.Random.Range(0, 10);
            var ranY = UnityEngine.Random.Range(0, 10);
            Vector3 position = new Vector3(player.transform.position.x + ranX, player.transform.position.y + ranY, player.transform.position.z);
            Instantiate(spell, position, Quaternion.identity);
        }
    }
    IEnumerator AttackDelay()
    {
        StartCoroutine(LockMovement(1.02f));
        animator.SetTrigger("Attack");
        animator.SetBool("Walk", false);
        yield return new WaitForSeconds(1.02f);

    }
    void StopAttack()
    {
       // animator.SetBool("Attack",false);
        animator.SetBool("Walk", true);
    }

    private IEnumerator LockMovement(float duration)
    {
        speed = 0;
        yield return new WaitForSeconds(duration);
        speed = originalSpeed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameManager.instance.player.TakeDamage(3);
        }
    }
}
