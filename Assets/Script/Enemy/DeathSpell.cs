using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSpell : MonoBehaviour
{
    private void Start()
    {
        Destroy(gameObject, 1f);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            collision.GetComponent<Player>().TakeDamage(1);
        }
    }
}
