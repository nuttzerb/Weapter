using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WormBoss : Enemy
{
    [HideInInspector] public float attackDistance;
    [HideInInspector] public float spellDistance;
    [SerializeField] GameObject projectile;
    [SerializeField] float projectileForce=5f;
    [SerializeField] GameObject FirePointAnchor;
    [SerializeField] Slider smallHealthBar;
    [SerializeField] GameObject portal;
    //[SerializeField] private GameObject spell;
    float originalSpeed;
    bool canCast = true;
    public bool enableCast = false;
    Vector2 spawnPos;
    protected override void Start()
    {
        base.Start();
        if (transform.localScale.z == 1)
        {
            GameManager.instance.bossHealthSlider.gameObject.SetActive(true);
        }
        else
        {
            smallHealthBar.gameObject.SetActive(true);
        }
        rb = GetComponent<Rigidbody2D>();
        attackDistance = base.stoppingDistance + 1.5f;
        spellDistance = base.stoppingDistance + 10;
        GetComponent<BoxCollider2D>().enabled = true;
        originalSpeed = speed;
        spawnPos = transform.Find("FirePointAnchor").position;
        speed *= 1/transform.localScale.z;
        maxHitpoint = (int)((float)maxHitpoint * transform.localScale.z);
        hitpoint = maxHitpoint;
    }

    protected override void Update()
    {
        base.Update();
        animator.SetBool("Walk", canMove);


        if (transform.localScale.z == 1)
        {
            GameManager.instance.bossHealthSlider.maxValue = maxHitpoint;
            GameManager.instance.bossHealthSlider.value = hitpoint;
        }
        else
        {
            smallHealthBar.maxValue = maxHitpoint;
            smallHealthBar.value = hitpoint;
        }

        BossMechanic();
    }
    protected override void Death()
    {
        speed = 0;
        GetComponent<BoxCollider2D>().enabled = false;
        if (transform.localScale.z == 1)
        {
            GameManager.instance.bossHealthSlider.gameObject.SetActive(false);
        }
        else
        {
            smallHealthBar.gameObject.SetActive(false);
        }
        animator.SetTrigger("Dead");
        Invoke(nameof(SpawnNewChildBoss), 1f);
        Destroy(gameObject, 1f);
        GameManager.instance.wormBossCount++;
        Debug.Log(GameManager.instance.wormBossCount);
        if(GameManager.instance.wormBossCount==15)
        {
            //GameObject.FindGameObjectWithTag("Finish").gameObject.SetActive(true);
            portal.SetActive(true);
        }
    }
    private void SpawnNewChildBoss()
    {
        Vector3 spawnPos;
        if (transform.localScale.z > .2f)
        {
            for (int i = 0; i < 2; i++)
            {
                if(i==1)
                {
                    spawnPos = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);
                }
                else
                {
                    spawnPos = new Vector3(transform.position.x -1, transform.position.y, transform.position.z);
                }
                var boss = Instantiate(gameObject, spawnPos, Quaternion.identity);
                var bossComponent = boss.GetComponent<Enemy>();
                boss.SetActive(true);
                bossComponent.transform.localScale = new Vector3(transform.localScale.x / 2, transform.localScale.y / 2, transform.localScale.z / 2);
                bossComponent.speed = originalSpeed;
            }
        }

    }
    private void BossMechanic()
    {
        if (Vector2.Distance(transform.position, base.player.position) <= spellDistance)
        {
            StartCoroutine(AttackDelay());
        }
        else if (Vector2.Distance(transform.position, base.player.position) >= spellDistance)
        {
            StopAttack();
        }
        if(enableCast)
        {
            FireAttack();
        }

    }
    IEnumerator AttackDelay()
    {
        //StartCoroutine(LockMovement(1.02f));
        animator.SetTrigger("Attack");
        animator.SetBool("Walk", false);
        //FireAttackDelay();
         yield return new WaitForSeconds(1.02f);
    }
    void StopAttack()
    {
        // animator.SetBool("Attack",false);
        animator.SetBool("Walk", true);
    }
    public void FireAttackDelay()
    {
        Invoke(nameof(FireAttack), 1.0f);
    }
    public void FireAttack()
    {
        if (player==null) player = GameObject.FindGameObjectWithTag("Player").transform;
        Vector2 spawnPos = FirePointAnchor.transform.position;
        if (player != null)
        {

            GameObject spell = Instantiate(projectile, spawnPos, Quaternion.identity);
            Vector2 playerPos = player.position;
            Vector2 direction = (playerPos - spawnPos).normalized; // lay gia tri
            var spellRb = spell.GetComponent<Rigidbody2D>();
            spellRb.velocity = direction * projectileForce; // van toc
        }
        enableCast = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
        //    GameManager.instance.player.TakeDamage(3);
        }
    }
}
