using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    [Header("Reference")]
    public static GameManager instance;
    public Player player;
    [SerializeField] VpadController vpadController;
    [Header("UI")]
    public GameObject canvas;
    public GameObject menuCanvas;
    public GameObject HUD;
    public FloatingTextManager floatingTextManager;
    public LevelSelection levelSelection;
    public GameObject characterMenu;
    [Header("Camera Shake")]
    public CameraShake cameraShake;
    public float duration=0.4f;
    public float magnitude=0.15f;
    [Header("Boss")]
    public Slider bossHealthSlider;
    [Header("Currency")]
    [Header("Characters Select")]
    public List<Sprite> playerSprites;
    public RuntimeAnimatorController[] playerAnimation;
    [SerializeField] public CharacterManager characterManager;
    [Header("Victory Menu")]
    public Text totalTime;
    public Text goldCollected;

    public int wormBossCount = 0;
    //resources


    /*  public Animator deadMenuAnimator;
      public Animator resultMenuAnimator;
      public Animator characterMenuAnimator;*/
    // public ResultMenu resultMenu;

    private void Awake()
    {
        if (GameManager.instance != null)
        {
            Destroy(gameObject);
            Destroy(player.gameObject);
            Destroy(floatingTextManager.gameObject);
            Destroy(cameraShake);
            Destroy(HUD);
            Destroy(canvas);
           // Destroy(characterMenu);
            return;
        }
         instance = this; // quan trong

        cameraShake = FindObjectOfType<CameraShake>().GetComponent<CameraShake>();
       // SavePlayer(); //reset player data
         LoadPlayer();
        DontDestroyOnLoad(canvas);
        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(player);
        DontDestroyOnLoad(cameraShake);
        DontDestroyOnLoad(HUD);
        DontDestroyOnLoad(menuCanvas);
        //DontDestroyOnLoad(characterMenu);
    }
    private void Update()
    {
        HideHUD();

    }
    public void HideHUD()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            HUD.SetActive(false);
        }
        else
        {
            HUD.SetActive(true);
        }
    }
    public void ShowText(string msg, int fontSize,  Color color, Vector3 position,Vector3 motion, float duration)
    {
        floatingTextManager.Show(msg, fontSize, color, position, motion, duration);
    }
    public void ResetPlayerStats()
    {
        player.hitpoint = player.maxHitpoint;
        player.playerHealthUI.SetMaxHealth(player.maxHitpoint);
        player.isAlive=true;
    }

    public void SavePlayer()
    {
        FileHandler.SavePlayer(player);
        Debug.Log("Game saved");
    }
    public void LoadPlayer()
    {
        PlayerData playerData = FileHandler.LoadPlayer();
        player.levels = playerData.levels;
        player.coins = playerData.coins;
        player.characterOwnedList = playerData.characters;
        Debug.Log("Load player data"+ "\nLevel: " + player.levels+ "\nCoins: " + player.coins + "\nCharacters Owned: " + player.GetTotalOwnedCharacter());
    }
    public VpadController GetVpadController()
    {
        return vpadController;
    }
}
