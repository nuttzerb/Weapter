using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSystem : MonoBehaviour
{
    private enum State
    {
        Idle,
        Active,
        BattleOver,
    }
    [SerializeField] List<Wave> waveArray;
    [SerializeField] BattleColliderTrigger colliderTrigger;
    [SerializeField] GameObject wallZone;
    [SerializeField] GameObject wallZonemMinimap;
    [SerializeField] GameObject chest;
    GameObject portal;
    private State state;
    private void Awake()
    {
        state = State.Idle;

    }

    // Start is called before the first frame update
    void Start()
    {
        if(wallZone!=null)
        {
            wallZone.SetActive(false);
            wallZonemMinimap.SetActive(false);
        }
        if (portal == null)
        {
            portal = GameObject.FindGameObjectWithTag("Portal");
            portal.SetActive(true);

        }
        colliderTrigger.OnPlayerEnterTrigger += ColliderTrigger_OnPlayerEnterTrigger;
        foreach (Wave wave in waveArray)
        {
            wave.Start();
        }
    }

    private void ColliderTrigger_OnPlayerEnterTrigger(object sender, System.EventArgs e)
    {
        if(state == State.Idle)
        {
            StartBattle();
            colliderTrigger.OnPlayerEnterTrigger -= ColliderTrigger_OnPlayerEnterTrigger;
        }
    }

    private void StartBattle()
    {
     //   Debug.Log("Start Battle");
        state = State.Active;
        StartCoroutine(GameManager.instance.cameraShake.Shake(GameManager.instance.duration, GameManager.instance.magnitude));
         
    }

    public List<SpawnPoint> GetSpawnPointList()
    {
        var list = new List<SpawnPoint>();
        foreach (var wave in waveArray)
        {
            foreach (var spawnPoint in wave.spawnPoints)
            {
                list.Add(spawnPoint);
            }
        }
        return list;
    }
    private void Update()
    {
        switch (state)
        {
            case State.Active:
                SetActiveCorridor(true);
                foreach (Wave wave in waveArray)
                {
                    wave.Update();
                }
                BattleOVer();
                break;
        }

  
    }

    private void SetActiveCorridor(bool value)
    {
        wallZone.SetActive(value);
        wallZonemMinimap.SetActive(value);
        portal.SetActive(!value);
    }

    private void BattleOVer()
    {
        if(state == State.Active)
        {
            if(AreWaveOver())
            {
                state = State.BattleOver;
                SetActiveCorridor(false);
                if (chest == null) return;
                else Instantiate(chest, transform.position, Quaternion.identity);

                //  Debug.Log("BattleOver");
            }
        }
    }

    private bool AreWaveOver()
    {
        foreach (Wave wave in waveArray)
        {
            if(wave.isWaveOver())
            {
                // wave is over
               // return true; //ket thuc 1 wave, chua duoc
            }
            else
            {
                //wave not over
                return false;
            }
        }
        return true;
    }
    public void AddCorridor()
    {
        wallZone = GameObject.FindGameObjectWithTag("Corridor");
        wallZonemMinimap = GameObject.FindGameObjectWithTag("CorridorMinimap");
        portal = GameObject.FindGameObjectWithTag("Portal");
    }
    [System.Serializable]
    private class Wave// : MonoBehaviour
    {
        [SerializeField] public List<SpawnPoint> spawnPoints;
        [SerializeField] public float timer;
        //[SerializeField] int numOfEnemies;
        private int numOfEnemies;
        public GameObject[] enemyInScene;

        public void Start()
        {
            numOfEnemies = spawnPoints.Count;
        }
        public void Update()
        {
        //    Debug.Log(enemyInScene.Length);
            if (timer>0)
            {
                timer -= Time.deltaTime;
            }
            {
                if (timer < 0)
                {
                    if(numOfEnemies>0)
                    {
                        SpawnEnemy();
                    }
                }
            }
            enemyInScene = GameObject.FindGameObjectsWithTag("Enemy");
       }
        public void SpawnEnemy()
        {
            foreach (SpawnPoint spawnPoint in spawnPoints)
            {
                spawnPoint.GetComponent<SpawnPoint>().Spawn();
                numOfEnemies--;
            }
        }

        public bool isWaveOver()
        {
            if (timer < 0)
            {
          //      Debug.Log(enemyInScene.Length);
                if (enemyInScene.Length > 0) return false;
                else return true;
            }
            else
            {
                return false;
            }
        }
        public float GetTimer()
        {
            return timer;
        }
    }
    
}
