using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrap : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Player")
        {
            collision.GetComponent<Player>().TakeDamage(1);
        }
        if(collision.tag=="Edge")
        {
            Debug.Log("spike destroyed");
            Destroy(gameObject);
        }
    }
}
