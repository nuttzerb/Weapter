using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VpadController : MonoBehaviour
{
    [SerializeField] EventTrigger _attackButtonEventTrigger;
    [SerializeField] EventTrigger _pickupButtonEventTrigger;
    [SerializeField] EventTrigger _skillButtonEventTrigger;
    public Button skillButtonEventTrigger  => _skillButtonEventTrigger.GetComponent<Button>();
    private bool isPressed;
    private bool isPickup;
    private bool isSkillPressed;
    private void Start()
    {
        EventTriggerExtensions.AddListener(_attackButtonEventTrigger, EventTriggerType.PointerDown, (eventData) => OnHoldingBasicAttack());
        EventTriggerExtensions.AddListener(_attackButtonEventTrigger, EventTriggerType.PointerUp, (eventData) => OnReleasingBasicAttack());
        EventTriggerExtensions.AddListener(_pickupButtonEventTrigger, EventTriggerType.PointerDown, (eventData) =>OnClickPickup(true));
        EventTriggerExtensions.AddListener(_pickupButtonEventTrigger, EventTriggerType.PointerUp, (eventData) => OnClickPickup(false));
        EventTriggerExtensions.AddListener(_skillButtonEventTrigger, EventTriggerType.PointerUp, (eventData) => OnClickSkill(true));     
    } 
    private void OnHoldingBasicAttack()
    {
        isPressed = true;

    }
    private void OnReleasingBasicAttack()
    {
        isPressed = false;
    }
    public bool IsPressed()
    {
        return isPressed;
    }
    public void OnClickPickup(bool value)
    {
        isPickup = value;
    }
    public bool IsPickup()
    {
        return isPickup;
    }
    public void SetPickupButtonInteracable(bool value)
    {
        _pickupButtonEventTrigger.gameObject.GetComponent<Button>().interactable=value;
    }
    public void OnClickSkill(bool value)
    {
        if(skillButtonEventTrigger.interactable)
        isSkillPressed = value;
    }
    public bool IsSkillPressed()
    {
        return isSkillPressed;
    }
}
