using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Joystick : MonoBehaviour
{
    [SerializeField] VpadController vpadController;
    [SerializeField] Player player;
    [SerializeField] float speed = 5;
    [SerializeField] Transform circle;
    [SerializeField] Transform outerCircle;
    [SerializeField] GameObject joystickIdle;
    [SerializeField] float dragDistance=50;
    [SerializeField] float speedMovementForHandler;
    [SerializeField] Text debugLog;
    private bool touchStart = false;
    private Vector2 pointA;
    private Vector2 pointB;
    private float widthAreaAllowForJoystick;
    private float heighAreaAllowForJoystick;
    int i;
    private void Start()
    {
        widthAreaAllowForJoystick = Screen.width * .5f;
        heighAreaAllowForJoystick = Screen.height* .5f;
    }
    private bool CheckAreaAllowForJoystick()
    {
        if(Input.mousePosition.x < widthAreaAllowForJoystick && Input.mousePosition.y< heighAreaAllowForJoystick)
        {
            return true;
        }
        return false;
    }
    private bool CheckTouchAreaForJoytick(int touchCount)
    {
        if (Input.GetTouch(touchCount).position.x < widthAreaAllowForJoystick && Input.GetTouch(touchCount).position.y < heighAreaAllowForJoystick)
        {
            return true;
        }   
        else if (Input.GetTouch(touchCount).position.x > widthAreaAllowForJoystick && Input.GetTouch(touchCount).position.y > heighAreaAllowForJoystick)
        {
            if (Input.GetTouch(0).position.x < widthAreaAllowForJoystick && Input.GetTouch(0).position.y < heighAreaAllowForJoystick)
            {
                i = 0;
                return true;
            }
        }
        else if(Input.touchCount==2)
        {
            if (Input.GetTouch(0).position.x < widthAreaAllowForJoystick && Input.GetTouch(0).position.y < heighAreaAllowForJoystick)
            {
                i = 0;
                return true;
            }
        }
        return false;
    }
    void Update()
    {
#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0) && CheckAreaAllowForJoystick())
        {
            pointA = Input.mousePosition;
            SetActiveJoystick(true);
            outerCircle.transform.position = pointA;
            touchStart = true;
        }
        if (Input.GetMouseButton(0))
        {
            pointB = Input.mousePosition;
        }
        else
        {
            touchStart = false;
        }
#elif UNITY_ANDROID 
        if (Input.touchCount >0 )
        {
            i = Input.touchCount-1;

            if (CheckTouchAreaForJoytick(i) )
            {
                var firstTouch = Input.GetTouch(i);
               
                switch (firstTouch.phase)
                {
                    case TouchPhase.Began:
                        {
                            pointA = firstTouch.position;
                            SetActiveJoystick(true);
                            outerCircle.transform.position = pointA;
                            touchStart = true;
                        }
                        break;
                    case TouchPhase.Moved:
                        {
                            pointB = firstTouch.position;
                        }
                        break;
                    case TouchPhase.Ended:
                        touchStart = false;
                        debugLog.text = "";
                        break;
                }
           }
        }

#endif
    }
    private void FixedUpdate()
    {
        if(touchStart)
        {
            Vector2 offset = pointB - pointA;
            Vector2 direction = Vector2.ClampMagnitude(offset, 1f);

            //player.lastMoveDirection = direction;
            MoveCharacter(direction);
            //move circle
            Vector2 targetPos = new Vector2(pointA.x +direction.x* dragDistance, pointA.y + direction.y * dragDistance);
            circle.transform.position = Vector2.Lerp(circle.transform.position, targetPos, speedMovementForHandler * Time.deltaTime);

            if (direction != Vector2.zero)
            {
                player.lastMoveDirection=direction;
            }
        }
        else
        {
            SetActiveJoystick(false);
        }
    }
    private void MoveCharacter(Vector2 direction)
    {
        player.transform.Translate(direction * speed * Time.deltaTime);
    }
    private void SetActiveJoystick(bool active)
    {
        circle.GetComponent<Image>().enabled = active;
        circle.gameObject.transform.GetChild(0).GetComponent<Image>().enabled = active;
        outerCircle.GetComponent<Image>().enabled = active;
        joystickIdle.gameObject.SetActive(!active);
    }
}
