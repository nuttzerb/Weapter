using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ProceduralGenerationAlgorithm
{
    public static HashSet<Vector2Int> SimpleRandomWalk(Vector2Int startPosition, int walkLength)
    {
        HashSet<Vector2Int> path = new HashSet<Vector2Int>();
        path.Add(startPosition);
        var previousPosition = startPosition;

        for (int i = 0; i < walkLength; i++)
        {
            var newPosition = previousPosition + Direction2D.GetRandomCardinalDirection();
            path.Add(newPosition);
            previousPosition = newPosition;
        }
        return path;
    }
    public static List<Vector2Int> RandomWalkCorridor(Vector2Int startPositon, int walkLength)
    {
        List<Vector2Int> corridor = new List<Vector2Int>();
        var direction = Direction2D.GetRandomCardinalDirection();
        var currentPosition = startPositon;
        corridor.Add(currentPosition);
        for (int i = 0; i < walkLength; i++)
        {
            currentPosition += direction;
            corridor.Add(currentPosition);
        }
        return corridor;
    }
    public static List<BoundsInt> BinarySpaceParitioning(BoundsInt spaceToSplit, int minWitdh, int minHeigh)
    {
        Queue<BoundsInt> roomsQueue = new Queue<BoundsInt>();
        List<BoundsInt> roomList = new List<BoundsInt>();
        roomsQueue.Enqueue(spaceToSplit);
        while (roomsQueue.Count > 0)
        {
            var room = roomsQueue.Dequeue();
            if(room.size.y >= minHeigh && room.size.x >= minWitdh)
            {
                if(Random.value>0.5f)
                {
                    if(room.size.y >=minHeigh *2)
                    {
                        SplitHorizontally(minHeigh, roomsQueue, room);
                    }
                    else if(room.size.x >= minWitdh*2)
                    {
                        SplitVertically(minWitdh, roomsQueue, room);
                    }
                    else if (room.size.x >= minWitdh && room.size.y>= minHeigh)
                    {
                        roomList.Add(room);
                    }
                }
                else
                {
                    if (room.size.x >= minWitdh * 2)
                    {
                        SplitVertically(minWitdh, roomsQueue, room);
                    }
                    else if (room.size.y >= minHeigh * 2)
                    {
                        SplitHorizontally(minHeigh, roomsQueue, room);
                    }
                    else if (room.size.x >= minWitdh && room.size.y >= minHeigh)
                    {
                        roomList.Add(room);
                    }
                }
            }
        }
        return roomList;
    }

    private static void SplitVertically(int minWitdh, Queue<BoundsInt> roomsQueue, BoundsInt room)
    {
        var xSplit = Random.Range(0, room.size.x);
        BoundsInt room1 = new BoundsInt(room.min, new Vector3Int(xSplit,room.size.y,room.size.z));
        BoundsInt room2 = new BoundsInt(new Vector3Int(room.min.x + xSplit, room.min.y, room.min.z),
            new Vector3Int(room.size.x - xSplit, room.size.y, room.size.z));

        roomsQueue.Enqueue(room1);
        roomsQueue.Enqueue(room2);
    }

    private static void SplitHorizontally(int minHeigh, Queue<BoundsInt> roomsQueue, BoundsInt room)
    {
        var ySplit = Random.Range(0, room.size.y);
        BoundsInt room1 = new BoundsInt(room.min, new Vector3Int(room.size.x, ySplit, room.size.z));
        BoundsInt room2 = new BoundsInt(new Vector3Int(room.min.x, room.min.y + ySplit, room.min.z), new Vector3Int(room.size.x, room.size.y - ySplit, room.size.z));
        roomsQueue.Enqueue(room1);
        roomsQueue.Enqueue(room2);
    }
}
public static class Direction2D
{
    public static List<Vector2Int> cardinalDirectionList = new List<Vector2Int>
    {
        new Vector2Int(0,1), //UP
        new Vector2Int(1,0), //RIGHT
        new Vector2Int(0,-1),//DOWN
        new Vector2Int(-1,0),//LEFT
    };
    public static List<Vector2Int> diagonalDirectionList = new List<Vector2Int>
    {
        new Vector2Int(1,1), //UP RIGHT
        new Vector2Int(1,-1),//RIGHT DOWN
        new Vector2Int(-1,-1),//LEFT DOWN
        new Vector2Int(-1,1), //UP LEFT
    };
    public static List<Vector2Int> eightDirectionList = new List<Vector2Int>
    {
        new Vector2Int(0,1), //UP
        new Vector2Int(1,1), //UP RIGHT
        new Vector2Int(1,0), //RIGHT
        new Vector2Int(1,-1),//RIGHT DOWN
        new Vector2Int(0,-1),//DOWN
        new Vector2Int(-1,-1),//LEFT DOWN
        new Vector2Int(-1,0),//LEFT
        new Vector2Int(-1,1), //UP LEFT

    };
    public static Vector2Int GetRandomCardinalDirection()
    {
        return cardinalDirectionList[Random.Range(0, cardinalDirectionList.Count)];
    }
}