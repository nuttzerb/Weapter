using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorridorFirstDungeonGeneration : SimpleRandomWalkDungeonGenerator
{
    [SerializeField] private int corridorLength=14;
    [SerializeField] private int corridorCount = 5;
    [SerializeField] [Range(.1f, 1f)] private float roomPercent;
    [SerializeField] public SimpleRandomWalkScriptableObject roomGenerationParameters;
    protected override void RunProceduralGeneration()
    {
        CorridorFirstGeneration();
    }

    private void CorridorFirstGeneration()
    {
        HashSet<Vector2Int> floorPosition = new HashSet<Vector2Int>();
        CreateCorridor(floorPosition);
        tilemapVisualizer.PaintFloorTiles(floorPosition);
        WallGenerator.CreateWalls(floorPosition,tilemapVisualizer);
    }
    private void CreateCorridor(HashSet<Vector2Int> foorPosition)
    {
        var currentPosition = startPosition;
        for (int i = 0; i < corridorCount; i++)
        {
            var corridor = ProceduralGenerationAlgorithm.RandomWalkCorridor(currentPosition, corridorLength);
            currentPosition = corridor[corridor.Count - 1];
            foorPosition.UnionWith(corridor);
        }
    }
    
}
