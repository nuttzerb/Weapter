using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralPrefabGeneration : MonoBehaviour
{
    [SerializeField] private GameObject battleSystemPrefab;

    [SerializeField] private GameObject enemySpawnPoint;

    [SerializeField] private GameObject chestPrefab;

    [SerializeField] private GameObject poitonPrefab;

    [SerializeField] private GameObject playerStartPoint;

    [SerializeField] private GameObject portal;

    [SerializeField] private GameObject trapPrefab;

    [SerializeField] private List<GameObject> weaponLoots;
    [HideInInspector] public bool isPlayerStartPointSpawned=false;
    
    List<GameObject> proceduralPrefabs = new List<GameObject>();
    private const int WALL_SIZE = 2;
    public void DespawnPrefabs()
    {
        foreach (var prefab in proceduralPrefabs)
        {
            DestroyImmediate(prefab);
        }
        proceduralPrefabs.Clear();
    }
    public void SpawnBattleSystem(BoundsInt room, int offset, List<Vector2Int> floor)
    {
        var offsetBox = offset * -.5f;
        offset += 2;
        var battleSystem = Instantiate(battleSystemPrefab, room.center, Quaternion.identity);
        battleSystem.transform.parent = gameObject.transform;
        if (battleSystem != null)
        {
            battleSystem.transform.GetChild(0).GetComponent<BoxCollider2D>().size = new Vector2(room.size.x - offset - WALL_SIZE-1, room.size.y - offset - WALL_SIZE + 3); // hard code fix size
            battleSystem.transform.GetChild(0).GetComponent<BoxCollider2D>().offset = new Vector2(0, offsetBox); 

        }
        proceduralPrefabs.Add(battleSystem);
        var wave = battleSystem.GetComponent<BattleSystem>();
        var SpawnPointList = wave.GetSpawnPointList();
        wave.AddCorridor();
        foreach (var spawnPoint in SpawnPointList)
        {
            int ran = Random.Range(0, floor.Count);
            spawnPoint.gameObject.transform.position = (Vector3Int)(floor[ran]);
        }
    }

    public void SpawnPoiton(List<Vector2Int> floor)
    {
        var poiton = Instantiate(poitonPrefab, (Vector3Int)(floor[Random.Range(0,floor.Count)]), Quaternion.identity);
        poiton.transform.parent = gameObject.transform;
        proceduralPrefabs.Add(poiton);

    }
    public void SpawnChest(List<Vector2Int> floor)
    {
        var poiton = Instantiate(chestPrefab, (Vector3Int)(floor[Random.Range(0, floor.Count)]), Quaternion.identity);
        poiton.transform.parent = gameObject.transform;
        proceduralPrefabs.Add(poiton);

    }
    public void SpawnWeaponLoot(List<Vector2Int> floor)
    {
        if (weaponLoots.Count < 3) return;
        var ran = Random.Range(0, weaponLoots.Count);
        var prefab = Instantiate(weaponLoots[ran], (Vector3Int)(floor[Random.Range(0,floor.Count)]), Quaternion.identity);
        weaponLoots.Remove(weaponLoots[ran]);
        prefab.transform.parent = gameObject.transform;
        proceduralPrefabs.Add(prefab);
    }
    public void SpawnTrap(List<Vector2Int> floor)
    {
        var prefab = Instantiate(trapPrefab, (Vector3Int)(floor[Random.Range(0,floor.Count)]), Quaternion.identity);
        prefab.transform.parent = gameObject.transform;
        proceduralPrefabs.Add(prefab);

    }
    public void SpawnPlayerSpawnPoint(Vector3 position)
    {
        if (isPlayerStartPointSpawned == true) return;
        var prefab = Instantiate(playerStartPoint, position, Quaternion.identity);
        prefab.transform.parent = gameObject.transform;
        proceduralPrefabs.Add(prefab);
        isPlayerStartPointSpawned = true;
    }  
    public void SpawnPortal(Vector3 position)
    {
        var prefab = Instantiate(portal, position, Quaternion.identity);
        prefab.transform.parent = gameObject.transform;
        proceduralPrefabs.Add(prefab); 
    }
    public List<GameObject> GetAllPrefabs()
    {
        return proceduralPrefabs;
    }
}
