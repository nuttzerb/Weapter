using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapVisualizer : MonoBehaviour
{
    [SerializeField] private Tilemap floorTilemap;

    [SerializeField] private TileBase floorTile; // floor sprite

    [SerializeField] private Tilemap wallTilemap;

    [SerializeField] private TileBase wallTop;// wall sprite
    [SerializeField] private TileBase wallruleTile;

    [SerializeField] private Tilemap corridorTilemap;

    [SerializeField] private TileBase corridorTile;

    [SerializeField] private Tilemap minimapWallTilemap;
    [SerializeField] private Tilemap corridorMinimapTilemap;
    public void PaintFloorTiles(IEnumerable<Vector2Int> floorPositions)
    {
        PainTitles(floorPositions, floorTilemap, wallruleTile);
    }
    public void PaintCorridorTiles(IEnumerable<Vector2Int> floorPositions)
    {
        PainTitles(floorPositions, corridorTilemap, corridorTile);
        PainTitles(floorPositions, corridorMinimapTilemap, corridorTile);
        
    }
    private void PainTitles(IEnumerable<Vector2Int> positions, Tilemap tilemap, TileBase tile)
    {
        foreach (var position in positions)
        {
            PaintSingleTile(tilemap, tile, position);
        }
    }

    internal void PainSingleBasicWall(Vector2Int position, string binaryType)
    {
        if(wallruleTile!=null)
        {
            PaintSingleTile(floorTilemap, wallruleTile, position);
        }


        if (wallTop!=null)
        {
            PaintSingleTile(wallTilemap, wallTop, position);
            PaintSingleTile(minimapWallTilemap, wallTop, position);
        }

    }

    private void PaintSingleTile(Tilemap tilemap, TileBase tile, Vector2Int position)
    {
        var titlePosition = tilemap.WorldToCell((Vector3Int)position);
        tilemap.SetTile(titlePosition, tile);

    }
    public void ClearTilemap()
    {
        floorTilemap.ClearAllTiles();
        wallTilemap.ClearAllTiles();
        corridorTilemap.ClearAllTiles();
        minimapWallTilemap.ClearAllTiles();
        corridorMinimapTilemap.ClearAllTiles();
    }

    internal void PaintSingleCornerWall(Vector2Int position, string binaryType)
    {
        if(wallruleTile!=null)
           PaintSingleTile(floorTilemap, wallruleTile, position);
        
    }
}
 