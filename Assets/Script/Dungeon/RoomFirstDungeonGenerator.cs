using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;


public class RoomFirstDungeonGenerator : SimpleRandomWalkDungeonGenerator
{
    [SerializeField] private int minRoomWitdh = 4;
    [SerializeField] private int minRoomHeight = 4;
    [SerializeField] private int dungeonWidth = 20;
    [SerializeField] private int dungeonHeight = 20;
    [SerializeField] [Range(0,10)] private int offset = 1;
    [SerializeField] private bool randomWalkRooms = false;

    //RoomData
    [SerializeField] private Dictionary<Vector2Int, HashSet<Vector2Int>> roomDictionary = new Dictionary<Vector2Int, HashSet<Vector2Int>>();
    [SerializeField] private HashSet<Vector2Int> floorPositionsData, corridorPositionsData;
    [SerializeField] private HashSet<Vector2Int> roomFloorPositionData;
    [SerializeField] private HashSet<Vector2Int> corridorOutsidePositionsData;

    //GimozData
    private List<Color> roomColors = new List<Color>();
    [SerializeField] private bool showRoomGizmos = false, showCorridorsGizmos;

    List<Vector2Int> floorPositionsDataList;// = new List<Vector2Int>(); // list to spawn prefab
    [SerializeField] ProceduralPrefabGeneration proceduralPrefabGeneration;
    [SerializeField] Player player;
    private Vector3 firstPosition;
    private void Awake()
    {
        RunProceduralGeneration();
    }
    protected override void RunProceduralGeneration()
    {
        floorPositionsDataList = new List<Vector2Int>();
        DespawnPrefab();
        CreateRooms();
    }
    private void Start()
    {
        StartCoroutine(SpawnFizeZoneTrap());

    }

    IEnumerator SpawnFizeZoneTrap()
    {
        yield return new WaitForSeconds(3f);
        if (SceneManager.GetActiveScene().name == "GeneDungeon Level 3" && floorPositionsDataList != null)
        {
            for (int i = 0; i < 8; i++)
            {
            proceduralPrefabGeneration.SpawnTrap(floorPositionsDataList);
            }
        }
        StartCoroutine(SpawnFizeZoneTrap());

    }
    private void CreateRooms()
    {
        if(GameObject.Find("Player")!=null)
        {
            player = GameObject.Find("Player").GetComponent<Player>();
        }
        var roomsList = ProceduralGenerationAlgorithm.BinarySpaceParitioning(new BoundsInt((Vector3Int)startPosition,
                                                                            new Vector3Int(dungeonWidth,dungeonHeight,0)),minRoomWitdh,minRoomHeight);
        HashSet<Vector2Int> floor = new HashSet<Vector2Int>();
        floorPositionsData = new HashSet<Vector2Int>();
        roomFloorPositionData = new HashSet<Vector2Int>();
        corridorOutsidePositionsData = new HashSet<Vector2Int>();
        if (randomWalkRooms)
        {
            floor = CreateRoomsRandomly(roomsList);
        }
        else
        {
            floor = CreateSimpleRooms(roomsList);
        }
        List<Vector2Int> roomCenters = new List<Vector2Int>();
        foreach (var room in roomsList)
        {
            var roomcenter = (Vector2Int)Vector3Int.RoundToInt(room.center);
            roomCenters.Add(roomcenter);
                floorPositionsData.UnionWith(roomDictionary[roomcenter]);
            }
        SpawnPortal((Vector2Int)Vector3Int.RoundToInt(firstPosition), roomCenters);
        HashSet<Vector2Int> corridors = ConnectRooms(roomCenters);
        floor.UnionWith(corridors);
        corridorOutsidePositionsData.UnionWith(floor);
        floorPositionsData.ExceptWith(corridorPositionsData); // get data except corridor
        corridorOutsidePositionsData.ExceptWith(roomFloorPositionData);
        foreach (var f in floorPositionsData)
        {
            floorPositionsDataList.Add(f);
        }
        for (int i = 0; i < 3; i++)
        {
            proceduralPrefabGeneration.SpawnPoiton(floorPositionsDataList);
            proceduralPrefabGeneration.SpawnWeaponLoot(floorPositionsDataList);
            proceduralPrefabGeneration.SpawnChest(floorPositionsDataList);
            
        }
        if(SceneManager.GetActiveScene().name== "GeneDungeon Level 1")
        {
            for (int i = 0; i < 15; i++)
            {
                proceduralPrefabGeneration.SpawnTrap(floorPositionsDataList);
            }
        }




        tilemapVisualizer.PaintFloorTiles(floor);
        //tilemapVisualizer.PaintFloorTiles(floorPositionsData);
        WallGenerator.CreateWalls(floor,tilemapVisualizer);
        tilemapVisualizer.PaintCorridorTiles(corridorOutsidePositionsData);
        if(player!=null)
        player.SpawnPlayerInPosition();
        Debug.Log("first: "+firstPosition); 
    }

    private HashSet<Vector2Int> CreateRoomsRandomly(List<BoundsInt> roomsList)
    {
        ClearRoomData();
        HashSet<Vector2Int> floor = new HashSet<Vector2Int>();
        for (int i = 0; i < roomsList.Count; i++)
        {
            var roomBounds = roomsList[i];
            var roomCenter = new Vector2Int(Mathf.RoundToInt(roomBounds.center.x), Mathf.RoundToInt(roomBounds.center.y));
            var roomFloor = RunRandomWalk(parameters, roomCenter);
            foreach (var position in roomFloor)
            {
                if(position.x >= (roomBounds.xMin + offset) && position.x <= (roomBounds.xMax - offset)
                    && position.y >=(roomBounds.yMin - offset) && position.y <= (roomBounds.yMax-offset))
                {
                    floor.Add(position);
                }

            }
            SaveRoomData(roomCenter, floor);
        }
        return floor;
    }

    private HashSet<Vector2Int> ConnectRooms(List<Vector2Int> roomCenters)
    {
        HashSet<Vector2Int> corridors = new HashSet<Vector2Int>();
        Debug.Log(roomCenters.Count);
        var currentRoomCenter = roomCenters[Random.Range(0, roomCenters.Count)];
        roomCenters.Remove(currentRoomCenter);

        while (roomCenters.Count>0)
        {
            Vector2Int closet = FindClosestPointTo(currentRoomCenter, roomCenters);
            roomCenters.Remove(closet);
            HashSet<Vector2Int> newCorridor = CreateCorridor(currentRoomCenter, closet);
            currentRoomCenter = closet;
            corridors.UnionWith(newCorridor); 
        }
        corridorPositionsData = new HashSet<Vector2Int>(corridors);
        return corridors;
    }

    private HashSet<Vector2Int> CreateCorridor(Vector2Int currentRoomCenter, Vector2Int destination)
    {
        HashSet<Vector2Int> corridor = new HashSet<Vector2Int>();
        var position = currentRoomCenter;
        corridor.Add(position);
        while(position.y != destination.y)
        {
            if(destination.y > position.y)
            {
                position += Vector2Int.up;
            }
            else if (destination.y < position.y)
            {
                position += Vector2Int.down;
            }
            corridor.Add(position);
        }
        while (position.x != destination.x)
        {
            if (destination.x > position.x)
            {
                position += Vector2Int.right;
            }
            else if (destination.x < position.x)
            {
                position += Vector2Int.left;
            }
            corridor.Add(position);

        }
        return corridor;
    }

    private Vector2Int FindClosestPointTo(Vector2Int currentRoomCenter, List<Vector2Int> roomCenters)
    {
        Vector2Int closet = Vector2Int.zero;
        float distance = float.MaxValue;
        foreach (var position in roomCenters)
        {
            float currentDistance = Vector2.Distance(position, currentRoomCenter);
            if(currentDistance<distance)
            {
                distance = currentDistance;
                closet = position;
            }
        }
        return closet;
    }
    private HashSet<Vector2Int> CreateSimpleRooms(List<BoundsInt> roomsList)
    {
        ClearRoomData();
        HashSet<Vector2Int> floor = new HashSet<Vector2Int>();
        List<Vector2Int> roomCenterList = new List<Vector2Int>();
        List<Vector2Int> indexFloor = new List<Vector2Int>();
        int numberRoom = 0;
        foreach (var room in roomsList)
        {
            indexFloor.Clear();
            for (int col = offset; col < room.size.x - offset; col++)
            {
                for (int row = 0; row < room.size.y - offset; row++)
                {
                    Vector2Int position = (Vector2Int)room.min + new Vector2Int(col, row);
                    floor.Add(position);
                    indexFloor.Add(position);
                    roomFloorPositionData.Add(position);
                }
            }
            var roomcenter = (Vector2Int)Vector3Int.RoundToInt(room.center);
            roomCenterList.Add(roomcenter);
            numberRoom++;

            switch (numberRoom)
            {
                case 1:
                    proceduralPrefabGeneration.SpawnPlayerSpawnPoint(room.center);
                    firstPosition = room.center;
                    break;
                default: proceduralPrefabGeneration.SpawnBattleSystem(room, offset, indexFloor);
                    break;
            }


            SaveRoomData((Vector2Int)Vector3Int.RoundToInt(room.center), floor);
        }

        return floor;
    }
    private void SpawnBattleSystem(List<BoundsInt> roomsList, List<Vector2Int> roomCenterList, List<Vector2Int> indexFloor)
    {
        foreach (var room in roomsList)
        {
            if (room.center != firstPosition && (Vector2Int)Vector3Int.RoundToInt(room.center) != (GetFarthestRoom((Vector2Int)Vector3Int.RoundToInt(firstPosition), roomCenterList)))
            {
                proceduralPrefabGeneration.SpawnBattleSystem(room, offset, indexFloor);
                Debug.Log("room.center: " + room.center);
            }
        }
    }

    private void ClearRoomData()
    {
        roomDictionary.Clear();
        roomColors.Clear();
        proceduralPrefabGeneration.isPlayerStartPointSpawned = false;
    }

    private void SaveRoomData(Vector2Int roomPosition, HashSet<Vector2Int> roomFloor)
    {
        roomDictionary[roomPosition] = roomFloor;
        roomColors.Add(UnityEngine.Random.ColorHSV());
    }

    private void DespawnPrefab()
    {
        proceduralPrefabGeneration.DespawnPrefabs();
    }

    private void SpawnPortal(Vector2Int startPosition, List<Vector2Int> roomCenter)
    {
        Vector2Int farthestRoom = GetFarthestRoom(startPosition, roomCenter);
        Vector3 portalPos = new Vector3(farthestRoom.x , farthestRoom.y );
        proceduralPrefabGeneration.SpawnPortal(portalPos);
 
    }

    private static Vector2Int GetFarthestRoom(Vector2Int startPosition, List<Vector2Int> roomCenter)
    {
        Vector2Int farthestRoom = startPosition;
        float farthestDistance = 0;
        foreach (var room in roomCenter)
        {
            var distance = Vector2Int.Distance(startPosition, room);

            if (distance > farthestDistance)
            {
                farthestDistance = distance;
                farthestRoom = room;
            }
        }
        Debug.Log("getfarthestRoom:"+ farthestRoom);
        return farthestRoom;
    }
}
