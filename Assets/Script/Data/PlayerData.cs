using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int levels;
    public int coins;
    public int[] characters;

    public PlayerData(Player player)
    {
        coins = player.coins;
        levels = player.levels;
        characters = player.characterOwnedList;
    }
}
