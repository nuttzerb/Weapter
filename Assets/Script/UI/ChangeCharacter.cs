using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCharacter : MonoBehaviour
{
    [SerializeField] GameObject characterMenu;

    private void OnTriggerStay2D(Collider2D collision)
    {
        collision.GetComponent<Rigidbody2D>().WakeUp();
        if (collision.tag == "Player" && GameManager.instance.GetVpadController().IsPickup())
        {
            GameManager.instance.GetVpadController().SetPickupButtonInteracable(true);
            OpenCharacterMenu();
        }
    }
    private void OpenCharacterMenu()
    {
        if(characterMenu==null)
        {
           var charMenu= GameObject.Find("CharacterMenu");
            charMenu.SetActive(true);
        }
        characterMenu.SetActive(true);
    }
}
