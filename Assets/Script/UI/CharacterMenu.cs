using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CharacterMenu : MonoBehaviour
{
    [SerializeField] Text characterText;
    public Text priceText;
    [SerializeField] Text coinText;
    private int currentCharacterSelection = 0;
    public Image characterSelectionSprite;
    private BuyingState buyingState;
    private void OnEnable()
    {
        InitSelection();
    }
    public void OnArrowClick(bool right)
    {
        if (right)
        {
            currentCharacterSelection++;
            if (currentCharacterSelection == GameManager.instance.characterManager.GetTotalCharacter())
                currentCharacterSelection = 0;
        }
        else
        {
            currentCharacterSelection--;
            if (currentCharacterSelection < 0)
                currentCharacterSelection = GameManager.instance.characterManager.GetTotalCharacter() - 1;
        }
        var characterData = GameManager.instance.characterManager.GetCharacterData(((Character)currentCharacterSelection).ToString());
        ChangeCharacterSprite(characterData);
    }

    private void ChangeCharacterSprite(CharacterScriptableObject characterData)
    {
        characterSelectionSprite.sprite = characterData.characterSprite;
        if (characterData == GameManager.instance.player.currentCharacter)
        {
            priceText.text = "SELECTED";
            buyingState = BuyingState.Used;
        }
        else if (GameManager.instance.player.characterOwnedList[currentCharacterSelection] == 0)
        {
            priceText.text = characterData.characterPrice;
            buyingState = BuyingState.NotOwned;
        }
        else
        {
            priceText.text = "SELECT";
            buyingState = BuyingState.Owned;
        }
    }

    private void OnSelectionChange()
    {
        coinText.text = GameManager.instance.player.coins.ToString();
        var characterData = GameManager.instance.characterManager.GetCharacterData(((Character)currentCharacterSelection).ToString());
        var characterPrice = int.Parse(characterData.characterPrice);
        if (buyingState != BuyingState.NotOwned)
        {
            ChangeCharacter(characterData);
        }
        else
        {
            if (GameManager.instance.player.coins >= characterPrice)
            {
                GameManager.instance.player.coins -= characterPrice;
                GameManager.instance.player.characterOwnedList[currentCharacterSelection] = 1;
                priceText.text = "SELECTED";
                GameManager.instance.SavePlayer();
                ChangeCharacter(characterData);
                coinText.text = GameManager.instance.player.coins.ToString();
            }
            else
            {
                Debug.Log("Not Enough Money");
            }

        }
    }

    private static void ChangeCharacter(CharacterScriptableObject characterData)
    {
        GameManager.instance.player.currentCharacter = characterData;
        GameManager.instance.player.animator.runtimeAnimatorController = characterData.characterAnim;
        GameManager.instance.player.spriteRenderer.sprite = characterData.characterSprite;
    }

    private void InitSelection()
    {
        coinText.text = GameManager.instance.player.coins.ToString();
        currentCharacterSelection = GameManager.instance.characterManager.GetCharacterIndex(GameManager.instance.player.currentCharacter.characterName);
        var characterData = GameManager.instance.characterManager.GetCharacterData(((Character)currentCharacterSelection).ToString());
        ChangeCharacterSprite(characterData);
    }
    public void OnBuyClick()
    {
        OnSelectionChange();
    }
    public void UpdateMenu()
    {

    }
    public void TurnOffCharacterMenu()
    {
        gameObject.SetActive(false);
    }
    public void TurnOnCharacterMenu()
    {
        gameObject.SetActive(true);
    }
}
public enum BuyingState
{
    Owned = 0,
    NotOwned,
    Used
}
